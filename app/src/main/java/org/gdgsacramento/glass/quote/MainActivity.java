package org.gdgsacramento.glass.quote;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends Activity {

    private TextView quoteTextView;
    private QuoteService quoteService = new QuoteService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        quoteTextView = (TextView) findViewById(R.id.textView);
        getNewQuote();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_get_quote) {
            getNewQuote();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_DPAD_CENTER) {
            openOptionsMenu();
            return true;
        } else {
            return super.onKeyDown(keycode, event);
        }
    }

    private void getNewQuote() {
        new GetQuoteTask().execute();
    }

    private class GetQuoteTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            return quoteService.getQuote();
        }

        @Override
        protected void onPostExecute(String quote) {
            quoteTextView.setText(quote);
        }
    }

}
